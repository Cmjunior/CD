void IntToBytes(uint16_t value, byte& HiByte, byte& LoByte)
{
  HiByte = value / 256;
  LoByte = value % 256;
}

void setDataBuffer(byte *byte_array)
{
  for(int i = 0; i <  LENGHTWORD; i++)
  {
    _data[i] = byte_array[i];
  }
}

byte GetChecksum(byte *byte_array)
{
  int sum = 0;

  for (int i = 0; i < (LENGHTBUFFER - 1); i++)
  {
    sum = sum + int(byte_array[i]);  
  }
  
  return byte(sum % 256);
}

void SendCommand(uint8_t action, uint16_t passive, uint16_t command)
{ 
  _last_buffer[HEADER_ID] = SYS_ID;
  _last_buffer[ACTION_ID] = action;
  _last_buffer[PASSIVE_ID] = passive;
  _last_buffer[COMMAND_LO_ID] = command;
  //IntToBytes(command, _last_buffer[COMMAND_HI_ID], _last_buffer[COMMAND_LO_ID]);

  for (int i = 0; i < LENGHTWORD; i++)
  {
    _last_buffer[4 + i] = _data[i];
  }

  _last_buffer[CHECKSUM_ID] = GetChecksum(_last_buffer);
  digitalWrite(5, HIGH);
  delay(100);
  serialPort.write(_last_buffer, LENGHTBUFFER);
  delay(5);
  digitalWrite(5, LOW);
}

void RemandCommand()
{
  digitalWrite(5, HIGH);
  delay(5);
  serialPort.write(_last_buffer, LENGHTBUFFER);
  delay(10);
  digitalWrite(5, LOW);
}

uint16_t ReadCommand()
{
  serialPort.readBytes(_buffer, LENGHTBUFFER);
  //_buffer[COMMAND_HI_ID] * 256 + 
  return _buffer[COMMAND_LO_ID];
}

bool isMe()
{
  if(_buffer[PASSIVE_ID] == DEVICE_ID){ return true;}
  else { return false;}
}

bool Checksum()
{
  if (GetChecksum(_buffer) == _buffer[CHECKSUM_ID])
  {
    return true;
  }
  else
  {
    return false;
  }
}

void cmdList(int cmd)
{
  //serialPortPC.println("switch");
  switch(cmd)
  {
    case ACK:
    {
      if(DEVICE_ID == MASTER_1)
      {
        int value = _buffer[DATA_BYTE_2]*256 + _buffer[DATA_BYTE_1];
        //RESEND LAST COMMAND
        serialPortPC.println("ACK");
        serialPortPC.println(value);
        
        if(_buffer[ACTION_ID] == SLAVE_1)
        {
           temperatura[1] = value; 
        }
        else
        {
           temperatura[2] = value;
        }
      }
      break;
    }
    case NACK:
    {      
      if(DEVICE_ID == MASTER_1)
      {
        //resend command to SLAVE
      }
      break;
    }
    case SW_ON_LED:
    {
      serialPortPC.println("SW_OFF_LED");
      //Liga led
      //serialPortPC.println("HIGH");
      digitalWrite(LED_BUILTIN, HIGH);
      if(DEVICE_ID != MASTER_1)
      {
        _data[0] = 1;
        _data[1] = 1;
        
        SendCommand(DEVICE_ID, MASTER_1, ACK);
      }
      break;
    }
    case SW_OFF_LED:
    {
      serialPortPC.println("SW_ON_LED");
      //serialPortPC.println("LOW");
      //Desliga LED
      digitalWrite(LED_BUILTIN, LOW);
      if(DEVICE_ID != MASTER_1)
      {
        _data[0] = 0;
        _data[1] = 0;
        
        SendCommand(DEVICE_ID, MASTER_1, ACK);
      }
      break;
    }
    case GET_SENSOR:
    {
      serialPortPC.println("GET_SENSOR");
      //Le porta analogica
      int value = analogRead(0);
      temperatura[0] = value;
      serialPortPC.println(value);
      if(DEVICE_ID != MASTER_1)
      {
        _data[0] = value%256;
        _data[1] = value/256;
        SendCommand(DEVICE_ID, MASTER_1, ACK); 
             
      }
      break;
    }
    case SW_OFF_LED_ADDRESS:
    {
      serialPortPC.println("SW_ON_LED_ADDRESS");
      // ENVIA COMANDO DE DESLIGAR LED PARA O DISPOSITIVO DE ENDEREÇO CONTIDO NO BYTE DE DATA
      SendCommand(MASTER_1, _buffer[DATA_BYTE_2], SW_OFF_LED);
      break;
    }
    case SW_ON_LED_ADDRESS:
    {
      serialPortPC.println("SW_OFF_LED_ADDRESS");
      // ENVIA COMANDO DE LIGAR LED PARA O DISPOSITIVO DE ENDEREÇO CONTIDO NO BYTE DE DATA
      SendCommand(MASTER_1, _buffer[DATA_BYTE_2], SW_ON_LED);
      break;
    } 
    case GET_SENSOR_ADDRESS:
    {
      serialPortPC.println("GET_SENSOR_ADDRESS");
      // ENVIA COMANDO DE LEITURA ANALOGICA PARA O DISPOSITIVO DE ENDEREÇO CONTIDO NO BYTE DE DATA
      SendCommand(MASTER_1, _buffer[DATA_BYTE_2], GET_SENSOR);
      break;
    }     
  }
}


