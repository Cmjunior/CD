#define DEVICE_ID                        MASTER_1
#define TIMEOUT                           500
#define serialPortPC                     Serial

#define RS485_RX                         14
#define RS485_TX                         12

/////////Defines ID Serial COM - command list///////////////////////////////////////////////
#define LENGHTWORD                        2                                               //
#define LENGHTBUFFER                      7                                               //
//===================== Action W/R - 1 Byte(1)   -  EX.: [1|0|0|0|0|0|0]==================//
#define SYS_ID                            97                                              // 
//===================== Action W/R - 1 Byte(1)   -  EX.: [0|1|0|0|0|0|0]==================//
#define PC_EXT                            106                                             // 
#define MASTER_1                          107                                             // 
#define SLAVE_1                           108                                             // 
#define SLAVE_2                           109                                             // 
/////////////////Define ID postion byte - Serial COM////////////////////////////////////////
#define HEADER_ID                         0                                               //
#define ACTION_ID                         1                                               //
#define PASSIVE_ID                        2                                               //
//#define COMMAND_HI_ID                     3                                               //
#define COMMAND_LO_ID                     3                                               //
#define DATA_BYTE_1                       4                                               //
#define DATA_BYTE_2                       5                                               //
#define CHECKSUM_ID                       6                                               //
////////////////////////////////////////////////////////////////////////////////////////////
//===================== Command    - 2 Bytes(23) -  EX.: [0|0|0|1|0|0|0]==================//
#define SW_OFF_LED_ADDRESS                114                                             //
#define SW_ON_LED_ADDRESS                 115                                             //
#define GET_SENSOR_ADDRESS                116                                             //
#define ACK                               117                                             // 
#define NACK                              118                                             // 
#define SW_ON_LED                         119                                             // 
#define SW_OFF_LED                        120                                             //
#define GET_SENSOR                        121                                             //
//===================== Data       - 2 Bytes(23) -  EX.: [0|0|0|1|1|0|0|0]================//

//===================== Checksum   - 1 Byte(5)   -  EX.: [0|0|0|0|0|0|0|1]================//
//Byte utilizado para verificar a soma de todos os outros bytes, o menos significativo da //
//soma é enviado. Feito para detectar error de transmissão de dados.                      //
////////////////////////////////////////////////////////////////////////////////////////////
