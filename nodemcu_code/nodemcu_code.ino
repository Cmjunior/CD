#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <stdio.h>

#include <SoftwareSerial.h>
#include "const.h"
#include "global.h"
#include "serialCom.h"
#include "wifiCom.h"

void setup() {
  // put your setup code here, to run once:
  serialPortPC.begin(115200);
  serialPort.begin(115200);
  //serialPort2.begin(4800);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(5, OUTPUT);
  digitalWrite(5, LOW); 

  WiFi.mode(WIFI_AP);
  WiFi.softAP(ssid, password);

  IPAddress myIP = WiFi.softAPIP();

  Serial.print ( "IP address: " );
  Serial.println ( WiFi.localIP() );

  stationConnectedHandler = WiFi.onSoftAPModeStationConnected(&onStationConnected);
  stationDisconnectedHandler = WiFi.onSoftAPModeStationDisconnected(&onStationDisconnected);
  //server.on ( "/", handleRoot );
  server.begin();
}

void loop() 
{
  WiFiClient client = server.available();
//  if(!client)
//  {
//    return;
//  }
//  while(!client.available())
//  {
//    delay(1);
//  }
 // server.handleClient();
  setLedWeb(&client);
  handleRoot(&client);
  
  serialEvent();
  serialEventPC();
}

void getCommand()
{
  if(Checksum)
  {
    int cmd = ReadCommand();
    if(isMe())
    {
      //serialPortPC.println("ISME");
      cmdList(cmd);
    }
    else
    {
      //serialPortPC.println("NOTME");
    }
  }
  else
  {
    serialPortPC.println("CHECKSUM");
    //ERRO CHECKSUM
    if(DEVICE_ID == MASTER_1)
    {
      
      while (serialPort.available()) 
      {
        serialPort.read();
      }
      //RESEND LAST COMMAND
    }
    else
    {
      //SEND NACK TO MASTER
    }
  }
}

void serialEvent() 
{
  unsigned long timoutCounter = millis();
  
  
  while (serialPort.available()) 
  {
    if(serialPort.available() >= LENGHTBUFFER)
    {
      timoutCounter = millis();
      //serialPortPC.println("SerialEvent");
      getCommand();
    }
    if((millis() - timoutCounter) > TIMEOUT)
    {
      while (serialPort.available()) 
      {
        serialPort.read();
      }
      break;
    }
  } 
}

void serialEventPC() 
{
  unsigned long timoutCounter = millis();
  while (serialPortPC.available()) 
  {
    if(serialPortPC.available() > 1)
    {
      serialPortPC.readBytes(_pc_buffer, 2);
      _buffer[DATA_BYTE_2] = _pc_buffer[1];
      cmdList(_pc_buffer[0]);
    }    
  }
}



