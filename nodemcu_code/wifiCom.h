void onStationConnected(const WiFiEventSoftAPModeStationConnected& evt) {
  Serial.print("Station connected");
}

void onStationDisconnected(const WiFiEventSoftAPModeStationDisconnected& evt) {
  Serial.print("Station disconnected");
}

void handleRoot(WiFiClient *client) 
{
  client->println("HTTP/1.1 200 OK");
  client->println("Content-Type: text/html");
  client->println("");
  client->println("<!DOCTYPE HTML>");  
  client->println("<html>");
  client->println("<head>");
 // client->println("<meta http-equiv='refresh' content='5'/>");
  client->println("<title>Comunicacao de dados</title>");
  client->println("<style>");
  client->println("body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }");
  client->println("</style>");
  client->println("</head>");
  client->println("<body>");
  client->println("<h1 style='text-align:center;'>Comunicacao de dados!</h1>");
  client->println("<br>");   
  client->println("<a href=\"/LED_1=ON\"\"><button>LED MASTER ON</button></a>");
  client->println("<a href=\"/LED_1=OFF\"\"><button>LED MASTER OFF</button></a>");
  client->println("<a href=\"/SENSOR=MASTER\"\"><button>TEMPERATURA MASTER</button></a>");
  client->println("<h2>");
  client->print(temperatura[0]);
  client->print("<h2>");
  client->println("</br>");
  client->println("</br>");
  client->println("<a href=\"/LED_2=ON\"\"><button>LED SLAVE_1 ON</button></a>");
  client->println("<a href=\"/LED_2=OFF\"\"><button>LED SLAVE_1 OFF</button></a>");
  client->println("<a href=\"/SENSOR=SLAVE_1\"\"><button>TEMPERATURA SLAVE_1</button></a>");
  client->println("<h3>");
  client->print(temperatura[1]);
  client->print("<h3>");
  client->println("</br>");
  client->println("<br>");
  client->println("<a href=\"/LED_3=ON\"\"><button>LED SLAVE_2 ON</button></a>");
  client->println("<a href=\"/LED_3=OFF\"\"><button>LED SLAVE_2 OFF</button></a>");
  client->println("<a href=\"/SENSOR=SLAVE_2\"\"><button>TEMPERATURA SLAVE_2</button></a>");
  client->println("<h4>");
  client->print(temperatura[2]);
  client->print("<h4>");
  client->println("</br>"); 
  
  client->println("</body>");
  client->println("</html>");
  
//  server.send ( 200, "text/html", temp );
}


void setLedWeb(WiFiClient *client)
{
  
  String request = client->readStringUntil('\r');
  Serial.println(request);
  client->flush();
  if(request.indexOf("/LED_1=ON") != -1)
  {
    cmdList(SW_OFF_LED);
  }
  if(request.indexOf("/LED_1=OFF") != -1)
  {
    cmdList(SW_ON_LED);
  }
  if(request.indexOf("/LED_2=ON") != -1)
  {
    _buffer[DATA_BYTE_2] = SLAVE_1;
    cmdList(SW_OFF_LED_ADDRESS);
  }
  if(request.indexOf("/LED_2=OFF") != -1)
  {
    _buffer[DATA_BYTE_2] = SLAVE_1;
    cmdList(SW_ON_LED_ADDRESS);
  }
  if(request.indexOf("/LED_3=ON") != -1)
  {
    _buffer[DATA_BYTE_2] = SLAVE_2;
    cmdList(SW_ON_LED_ADDRESS);
  }
  if(request.indexOf("/LED_3=OFF") != -1)
  {
    _buffer[DATA_BYTE_2] = SLAVE_2;
    cmdList(SW_OFF_LED_ADDRESS);
  }

  if(request.indexOf("/SENSOR=MASTER") != -1)
  {
    cmdList(GET_SENSOR);
  }

  if(request.indexOf("/SENSOR=SLAVE_1") != -1)
  {
    _buffer[DATA_BYTE_2] = SLAVE_1;
    cmdList(GET_SENSOR_ADDRESS);
  }

  if(request.indexOf("/SENSOR=SLAVE_2") != -1)
  {
    _buffer[DATA_BYTE_2] = SLAVE_2;
    cmdList(GET_SENSOR_ADDRESS);
  }
}

