#include "const.h"
#include "global.h"
#include "serialCom.h"

void setup() {
  // put your setup code here, to run once:
  serialPortPC.begin(115200);
  serialPort.begin(115200);
  //serialPort2.begin(4800);
  pinMode(13, OUTPUT);
  pinMode(5, OUTPUT);
  digitalWrite(5, LOW); 

  
}

void loop() 
{
  // put your main code here, to run repeatedly:
  serialEvent2();
  serialEventPC();
}

void getCommand()
{
  if(Checksum)
  {
    int cmd = ReadCommand();
    if(isMe())
    {
      //serialPortPC.println("ISME");
      cmdList(cmd);
    }
    else
    {
      //serialPortPC.println("NOTME");
    }
  }
  else
  {
    serialPortPC.println("CHECKSUM");
    //ERRO CHECKSUM
    if(DEVICE_ID == MASTER_1)
    {
      
      while (serialPort.available()) 
      {
        serialPort.read();
      }
      //RESEND LAST COMMAND
    }
    else
    {
      //SEND NACK TO MASTER
    }
  }
}

void cmdList(int cmd)
{
  //serialPortPC.println("switch");
  switch(cmd)
  {
    case ACK:
    {
      if(DEVICE_ID == MASTER_1)
      {
        int value = _buffer[DATA_BYTE_2]*256 + _buffer[DATA_BYTE_1];
        //RESEND LAST COMMAND
        serialPortPC.println("ACK");
        serialPortPC.println(value);
      }
      break;
    }
    case NACK:
    {      
      if(DEVICE_ID == MASTER_1)
      {
        //resend command to SLAVE
      }
      break;
    }
    case SW_ON_LED:
    {
      serialPortPC.println("SW_OFF_LED");
      //Liga led
      //serialPortPC.println("HIGH");
      digitalWrite(13, HIGH);
      if(DEVICE_ID != MASTER_1)
      {
        _data[0] = 1;
        _data[1] = 1;
        
        SendCommand(DEVICE_ID, MASTER_1, ACK);
      }
      break;
    }
    case SW_OFF_LED:
    {
      serialPortPC.println("SW_ON_LED");
      //serialPortPC.println("LOW");
      //Desliga LED
      digitalWrite(13, LOW);
      if(DEVICE_ID != MASTER_1)
      {
        _data[0] = 0;
        _data[1] = 0;
        
        SendCommand(DEVICE_ID, MASTER_1, ACK);
      }
      break;
    }
    case GET_SENSOR:
    {
      serialPortPC.println("GET_SENSOR");
      //Le porta analogica
      int value = analogRead(0);
      
      serialPortPC.println(value);
      if(DEVICE_ID != MASTER_1)
      {
        _data[0] = value%256;
        _data[1] = value/256;
        SendCommand(DEVICE_ID, MASTER_1, ACK); 
             
      }
      break;
    }
    case SW_OFF_LED_ADDRESS:
    {
      serialPortPC.println("SW_ON_LED_ADDRESS");
      // ENVIA COMANDO DE DESLIGAR LED PARA O DISPOSITIVO DE ENDEREÇO CONTIDO NO BYTE DE DATA
      SendCommand(MASTER_1, _buffer[DATA_BYTE_2], SW_OFF_LED);
      break;
    }
    case SW_ON_LED_ADDRESS:
    {
      serialPortPC.println("SW_OFF_LED_ADDRESS");
      // ENVIA COMANDO DE LIGAR LED PARA O DISPOSITIVO DE ENDEREÇO CONTIDO NO BYTE DE DATA
      SendCommand(MASTER_1, _buffer[DATA_BYTE_2], SW_ON_LED);
      break;
    } 
    case GET_SENSOR_ADDRESS:
    {
      serialPortPC.println("GET_SENSOR_ADDRESS");
      // ENVIA COMANDO DE LEITURA ANALOGICA PARA O DISPOSITIVO DE ENDEREÇO CONTIDO NO BYTE DE DATA
      SendCommand(MASTER_1, _buffer[DATA_BYTE_2], GET_SENSOR);
      break;
    }     
  }
}

void serialEvent2() {
  unsigned long timoutCounter = millis();
  
  
  while (serialPort.available()) 
  {   
    if(serialPort.available() > LENGHTWORD)
    {
      timoutCounter = millis();
      //serialPortPC.println("SerialEvent");
      getCommand();
    }
    if((millis() - timoutCounter) > TIMEOUT)
    {
      while (serialPort.available()) 
      {
        serialPort.read();
      }
      break;
    }
  } 
}

void serialEventPC() {
  unsigned long timoutCounter = millis();
  while (serialPortPC.available()) 
  {
    if(serialPortPC.available() > 1)
    {
      serialPortPC.readBytes(_pc_buffer, 2);
      _buffer[DATA_BYTE_2] = _pc_buffer[1];
      cmdList(_pc_buffer[0]);
    }    
  }
}



