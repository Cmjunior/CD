void IntToBytes(uint16_t value, byte& HiByte, byte& LoByte)
{
  HiByte = value / 256;
  LoByte = value % 256;
}

void setDataBuffer(byte *byte_array)
{
  for(int i = 0; i <  LENGHTWORD; i++)
  {
    _data[i] = byte_array[i];
  }
}

byte GetChecksum(byte *byte_array)
{
  int sum = 0;

  for (int i = 0; i < (LENGHTBUFFER - 1); i++)
  {
    sum = sum + int(byte_array[i]);  
  }
  
  return byte(sum % 256);
}

void SendCommand(uint8_t action, uint16_t passive, uint16_t command)
{ 
  _last_buffer[HEADER_ID] = SYS_ID;
  _last_buffer[ACTION_ID] = action;
  _last_buffer[PASSIVE_ID] = passive;
  _last_buffer[COMMAND_LO_ID] = command;
  //IntToBytes(command, _last_buffer[COMMAND_HI_ID], _last_buffer[COMMAND_LO_ID]);

  for (int i = 0; i < LENGHTWORD; i++)
  {
    _last_buffer[4 + i] = _data[i];
  }

  _last_buffer[CHECKSUM_ID] = GetChecksum(_last_buffer);
  digitalWrite(5, HIGH);
  delay(100);
  serialPort.write(_last_buffer, LENGHTBUFFER);
  delay(5);
  digitalWrite(5, LOW);
}

void RemandCommand()
{
  digitalWrite(5, HIGH);
  delay(5);
  serialPort.write(_last_buffer, LENGHTBUFFER);
  delay(10);
  digitalWrite(5, LOW);
}

uint16_t ReadCommand()
{
  serialPort.readBytes(_buffer, LENGHTBUFFER);
  //_buffer[COMMAND_HI_ID] * 256 + 
  return _buffer[COMMAND_LO_ID];
}

bool isMe()
{
  if(_buffer[PASSIVE_ID] == DEVICE_ID){ return true;}
  else { return false;}
}

bool Checksum()
{
  if (GetChecksum(_buffer) == _buffer[CHECKSUM_ID])
  {
    return true;
  }
  else
  {
    return false;
  }
}



